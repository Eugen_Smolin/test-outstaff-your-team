import { Navbar } from './components/Navbar';
import { Currencies } from './components/Currencies';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Navbar />
        <Currencies />
      </header>
    </div>
  );
}

export default App;
