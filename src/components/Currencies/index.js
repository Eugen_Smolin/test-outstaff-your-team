import { useState, useMemo } from 'react';

import { DATA_JSON, CURRENCIES } from '../../utils/const';
import './styled.css';

const includesElInArray = (arr, id) => arr.includes(id);

export const Currencies = () => {
  const [curCurrency, setCurCurrency] = useState(CURRENCIES.USDT);
  const [activeIdEl, setActiveIdEl] = useState([]);

  const { data, currencies_pairs } = DATA_JSON;

  const onChangeCurrency = (event) => {
    setCurCurrency(event.target.value);
  };

  const onSelectAll = () => {
    setActiveIdEl(data.map((el) => el.id));
  };

  const onResetAll = () => {
    setActiveIdEl([]);
  };

  const onChooseItem = (el) => () => {
    if (includesElInArray(activeIdEl, el.id)) {
      setActiveIdEl((prev) => prev.filter((id) => id !== el.id));
    } else {
      setActiveIdEl((prev) => [...prev, el.id]);
    }
  };

  const renderList = () => {
    return data.map((el) => (
      <div
        key={el.id}
        className={`App-item ${includesElInArray(activeIdEl, el.id) ? 'App-item--active' : ''}`}
        onClick={onChooseItem(el)}
      >
        <div>{el.id}</div>
        <div>{el.price}</div>
        <div>{el.currency}</div>
      </div>
    ));
  };

  const renderOptions = () => {
    return Object.values(CURRENCIES).map((el) => (
      <option key={el} value={el}>{el}</option>
    ));
  };

  const totalItems = useMemo(() => {
    const arrFiltered = data.filter((el) => activeIdEl.includes(el.id));

    if (arrFiltered.length) {
      return arrFiltered.reduce((accumulator, currentValue) => {
        const currenciesPairs = currencies_pairs[`${currentValue.currency}-${curCurrency}`];
        const price = currenciesPairs ? currentValue.price * currenciesPairs : currentValue.price;
        return accumulator + price;
      }, 0);
    } else {
      return 0;
    }
  }, [curCurrency, activeIdEl]);

  return (
    <>
      <h2>Task #2</h2>
      <div className="App-head">
        <div className="App-head__item">
          Currency:
          <select defaultValue={curCurrency} onChange={onChangeCurrency}>
            {renderOptions()}
          </select>
        </div>
        <div className="App-head__item">
          {data.length === activeIdEl.length ? (
            <button onClick={onResetAll}>
              Reset All
            </button>
          ) : (
            <button onClick={onSelectAll}>
              Select All
            </button>
          )}
        </div>
      </div>

      {renderList()}

      <div className="App-total">
        Total: {totalItems}
      </div>
    </>
  );
};
