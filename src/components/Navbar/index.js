import './styled.css';

const list = ['First', 'Second', 'Some Third Text'];

export const Navbar = () => {
  const renderList = () => {
    return list.map((el) => (
      <li key={el} className="App-li">{el}</li>
    ));
  };

  return (
   <>
     <h2>Task #1</h2>
     <ul className="App-navbar">
       {renderList()}
     </ul>
   </>
  );
};
